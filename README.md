# dvach api provider
Just basic classes for loading posts and threads from dvach. Maybe be suitable to someone. Written by me at free time.

## Used stuff
OkHTTP for requests to API and jackson for parsing json to pojo objects
## Troubles.
Project is single thread.
## Architecture
There are 5 basic classed which represents attachments, posts, boards, threads and dvach itself. For loading some performance improvements there are lazy loading used.
