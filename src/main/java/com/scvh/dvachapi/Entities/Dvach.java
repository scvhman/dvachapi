package com.scvh.dvachapi.Entities;

import java.util.HashMap;

/**
 * Класс является "двачем"
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Dvach {

    private HashMap<String, Board> boards;

    public HashMap<String, Board> getBoards() {
        return boards;
    }

    public void setBoards(HashMap<String, Board> boards) {
        this.boards = boards;
    }

}
