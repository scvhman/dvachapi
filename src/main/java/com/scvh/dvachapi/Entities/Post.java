package com.scvh.dvachapi.Entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Класс является абстрацией поста
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Post {

    @JsonProperty("banned")
    private boolean isBanned;
    @JsonProperty("closed")
    private boolean isClosed;
    @JsonProperty("op")
    private boolean isOp;
    @JsonProperty("sticky")
    private boolean isSticky;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("email")
    private String email;
    @JsonProperty("name")
    private String name;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("num")
    private int postId;

    private DateTime lastViewed;
    private DateTime postedOn;

    @JsonProperty("files")
    private ArrayList<Attachment> attachments;

    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean isBanned) {
        this.isBanned = isBanned;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    public boolean isOp() {
        return isOp;
    }

    public void setOp(boolean isOp) {
        this.isOp = isOp;
    }

    public boolean isSticky() {
        return isSticky;
    }

    public void setSticky(boolean isSticky) {
        this.isSticky = isSticky;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public DateTime getLastViewed() {
        return lastViewed;
    }

    @JsonProperty("lasthit")
    public void setLastViewed(long lasthit) {
        DateTime time = new DateTime(lasthit * 1000L);
        this.lastViewed = time;
    }

    public DateTime getPostedOn() {
        return postedOn;
    }

    @JsonProperty("timestamp")
    public void setPostedOn(long timestamp) {
        DateTime time = new DateTime(timestamp * 1000L);
        this.postedOn = time;
    }

    public ArrayList<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<Attachment> attachments) {
        this.attachments = attachments;
    }

}
