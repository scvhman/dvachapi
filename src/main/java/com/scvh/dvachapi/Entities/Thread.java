package com.scvh.dvachapi.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Класс является абстрацией доски
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Thread {

    @JsonProperty("comment")
    private String comment;
    @JsonProperty("title")
    private String subject;
    @JsonProperty("num")
    private int threadId;
    @JsonProperty("posts_count")
    private int postCount;
    @JsonProperty("unique_posters")
    private int uniquePosters;

    private DateTime threadTime;
    private DateTime lastViewed;

    private ArrayList<Post> posts;
    private Post opPost;

    public Post getOpPost() {
        return opPost;
    }

    public void setOpPost(Post opPost) {
        this.opPost = opPost;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getUniquePosters() {
        return uniquePosters;
    }

    public void setUniquePosters(int uniquePosters) {
        this.uniquePosters = uniquePosters;
    }

    public DateTime getThreadTime() {
        return threadTime;
    }

    @JsonProperty("timestamp")
    public void setThreadTime(long timestamp) {
        threadTime = new DateTime(timestamp * 1000L);
    }

    public DateTime getLastViewed() {
        return lastViewed;
    }

    @JsonProperty("lasthit")
    public void setLastViewed(long lasthit) {
        lastViewed = new DateTime(lasthit * 1000L);
    }

}
