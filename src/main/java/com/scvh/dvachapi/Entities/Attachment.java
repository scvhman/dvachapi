package com.scvh.dvachapi.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Класс является абстрацией файлов в посте
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Attachment {

    @JsonProperty("cloud")
    private boolean isNSFW;
    @JsonProperty("nsfw")
    private boolean isInCloud;
    @JsonProperty("height")
    private int height;
    @JsonProperty("width")
    private int width;
    @JsonProperty("size")
    private int size;
    @JsonProperty("type")
    private int type;

    @JsonProperty("name")
    private String name;
    @JsonProperty("md5")
    private String md5;

    public boolean isNSFW() {
        return isNSFW;
    }

    public void setNSFW(boolean NSFW) {
        isNSFW = NSFW;
    }

    public boolean isInCloud() {
        return isInCloud;
    }

    public void setInCloud(boolean inCloud) {
        isInCloud = inCloud;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }
}
