package com.scvh.dvachapi.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.scvh.dvachapi.Workers.LazyLoadLoader;

import java.util.ArrayList;

/**
 * Класс является абстрацией доски
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Board {

    @JsonProperty("sage")
    private boolean sageAllowed;
    @JsonProperty("tripcodes")
    private boolean tripcodesAllowed;
    @JsonProperty("enable_posting")
    private boolean postingAllowd;
    @JsonProperty("enable_thread_tags")
    private boolean threadTags;
    @JsonProperty("enable_likes")
    private boolean likesAllowed;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("category")
    private String category;
    @JsonProperty("default_name")
    private String defaultName;
    @JsonProperty("pages")
    private int pages;
    @JsonProperty("bump_limit")
    private int bumpLimit;

    /**
     * Использую лист с id тредов вместо объектов с тредами для увеличения перформанса
     * Если же создавать сами треды, то даже при условии запила треда с подзагрузкой постов все будет работать уж очень медленно
     */
    private ArrayList<Thread> threads;

    private LazyLoadLoader loadLoader = new LazyLoadLoader();

    public boolean isSageAllowed() {
        return sageAllowed;
    }

    public boolean isTripcodesAllowed() {
        return tripcodesAllowed;
    }

    public boolean isPostingAllowed() {
        return postingAllowd;
    }

    public boolean isThreadTags() {
        return threadTags;
    }

    public boolean isLikesAllowed() {
        return likesAllowed;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public int getPages() {
        return pages;
    }

    public int getBumpLimit() {
        return bumpLimit;
    }

    public ArrayList<Thread> getThreads() {
        if (threads == null) {
            threads = loadLoader.lazyLoadThreads(this);
        }
        return threads;
    }

    public void setThreads(ArrayList<Thread> threads) {
        this.threads = threads;
    }
}
