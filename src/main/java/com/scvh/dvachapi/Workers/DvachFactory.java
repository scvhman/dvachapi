package com.scvh.dvachapi.Workers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scvh.dvachapi.Entities.*;
import com.scvh.dvachapi.Entities.Thread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Класс является фабрикой позволяющей получать что-либо с двача
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class DvachFactory {

    private String request;
    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    private OkHTTPWorker HTTPworker = new OkHTTPWorker();
    private JsonNode response;

    /**
     * Метод "строит" пост
     *
     * @param board      - id доски
     * @param postNumber - id поста
     * @return Post - пост
     */
    public Post buildPost(String board, int postNumber) {
        request = "https://2ch.hk/makaba/mobile.fcgi?task=get_post&board=" + board + "&post=" + postNumber;
        Post[] post = new Post[0]; // массив тут связан с особенностями работы двачеапи
        response = HTTPworker.work(request);
        try {
            post = mapper.readValue(response.traverse(), Post[].class);
            if (response.get(0).get("files") != null) {
                JsonNode inners = mapper.readTree(response.get(0).get("files").traverse());
                ArrayList<Attachment> attachments = mapper.readValue(inners.traverse(), new TypeReference<ArrayList<Attachment>>() {
                });
                post[0].setAttachments(attachments);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return post[0]; // массив тут связан с особенностями работы двачеапи
    }

    /**
     * Метод "строит" тред
     *
     * @param board    - id доски
     * @param threadId - id треда
     * @return thread - тред
     */
    public Thread buildThread(String board, int threadId) {
        Thread thread = new Thread();
        request = "https://2ch.hk/" + board + "/res/" + threadId + ".json";
        response = HTTPworker.work(request);
        try {
            thread = mapper.readValue(response.traverse(), Thread.class);
            ArrayList<Post> posts = mapper.readValue(response.get("threads").get(0).get("posts").traverse()
                    , new TypeReference<ArrayList<Post>>() {
                    });
            thread.setPosts(posts);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Post opPost = buildPost(board, threadId);
        thread.setOpPost(opPost);
        //Так прихоится делать из-за всратого апи тредов
        thread.setComment(opPost.getComment());
        thread.setThreadId(opPost.getPostId());
        thread.setThreadTime(opPost.getPostedOn().getMillis());
        thread.setLastViewed(opPost.getLastViewed().getMillis());
        return thread;
    }

    /**
     * Метод "строит" абстракцию двача с списком всех досок
     *
     * @return dvach - двач
     */
    public Dvach buildDvach() {
        request = "https://2ch.hk/makaba/mobile.fcgi?task=get_boards";
        Dvach dvach = new Dvach();
        response = HTTPworker.work(request);
        Iterator iterator = response.elements();
        JsonNode innerNode;
        HashMap<String, Board> dvachBoards = new HashMap<>();
        //Двачик возвращает массив внутри массива поэтому приходится запиливать такую конструкцию
        //Джексон искоропки не умеет такое парсить
        while (iterator.hasNext()) {
            innerNode = (JsonNode) iterator.next();
            try {
                ArrayList<Board> dvachEntity = mapper.readValue(innerNode.traverse(), new TypeReference<ArrayList<Board>>() {
                });
                Iterator<Board> dvachIterator = dvachEntity.iterator();
                while (dvachIterator.hasNext()) {
                    Board board = dvachIterator.next();
                    dvachBoards.put(board.getId(), board);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dvach.setBoards(dvachBoards);
        return dvach;
    }

    /**
     * Метод "строит" доску
     *
     * @param boardName - id доски
     * @return board - доска
     */
    public Board buildBoard(String boardName) {
        ArrayList<Thread> threads = new ArrayList<>();
        Board board = buildDvach().getBoards().get(boardName);
        LazyLoadLoader loader = new LazyLoadLoader();
        threads = loader.lazyLoadThreads(board);
        board.setThreads(threads);
        return board;
    }
}
