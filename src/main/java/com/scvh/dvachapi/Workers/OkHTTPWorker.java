package com.scvh.dvachapi.Workers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Класс является шаблоном позволяющим отсылать http запросы и парсить их в JsonNode
 * СОЗДАНО ДЛЯ ПРЕДОТВРАЩЕНИЯ ДУБЛИРОВАНИЯ КОДА
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class OkHTTPWorker {

    private String answer;
    private JsonNode node;
    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


    /**
     * Метод возвращает JsonObject из строки с жсоном
     * @param request - запрос
     * @return node - полученный жсон
     */
    public JsonNode work(String request) {
        OkHttpClient client = new OkHttpClient();
        Request httpRequest = new Request.Builder().url(request).build();
        try {
            Response response = client.newCall(httpRequest).execute();
            answer = response.body().string();
            node = mapper.readTree(answer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return node;
    }

}
