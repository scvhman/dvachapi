package com.scvh.dvachapi.Workers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scvh.dvachapi.Entities.Board;
import com.scvh.dvachapi.Entities.Thread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Класс служит для "ленивой! подзагрузки списков тредов.
 * СДЕЛАНО ДЛЯ ОПТИМИЗАЦИИ ПЕРФОРМАНСА
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class LazyLoadLoader {

    private String request;
    private OkHTTPWorker httpWorker = new OkHTTPWorker();
    private ArrayList<Thread> threads = new ArrayList<>();
    private JsonNode results;
    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


    /**
     * Метод предназначен для ленивой подзагрузки списка тредов в доске.
     * Создано для перформанса
     * @param board - доска
     * @return threads - список тредов находящихся на доске
     */
    public ArrayList<Thread> lazyLoadThreads(Board board) {
        OkHTTPWorker httpWorker = new OkHTTPWorker();
        request = "https://2ch.hk/" + board.getId() + "/threads.json";
        results = httpWorker.work(request).get("threads");
        Iterator<JsonNode> iterator = results.iterator();
        while (iterator.hasNext()) {
            try {
                Thread thread = mapper.readValue(iterator.next().traverse(), Thread.class);
                threads.add(thread);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return threads;
    }

}
